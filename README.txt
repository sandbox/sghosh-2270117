CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

*******************************************************************************

INTRODUCTION
------------
The AdRoll Smartpixel Integration module is a simple module for integrating
adroll smartpixel js into your drupal pages without touching code. All you need
are the Advertisable ID and the Pixel ID from AdRoll.


REQUIREMENTS
------------
This module is not dependent on any other contrib module. However you will need
the Advertisable ID and the Pixel ID from AdRoll to render the js.


INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


CONFIGURATION
-------------
 * Configuration includes simply supplying the Advertisable ID and the Pixel ID
in the admin configuration page - admin/config/search/adroll


MAINTAINERS
-----------
Current maintainers:
 * Somedutta Ghosh (sghosh) - https://drupal.org/user/1766712
