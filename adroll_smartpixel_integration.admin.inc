<?php

/**
 * @file
 * Caontains the callback function for the admin settings form.
 */

/**
 * Callback function for admin configuration settings for the module.
 *
 * Stores the
 */
function adroll_smartpixel_integration_form($form, &$form_state) {
  $form = array();

  $form['adroll_smartpixel_integration_adv_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Advertisable ID'),
    '#default_value' => variable_get('adroll_smartpixel_integration_adv_id', ''),
  );
  $form['adroll_smartpixel_integration_pix_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Pixel ID'),
    '#default_value' => variable_get('adroll_smartpixel_integration_pix_id', ''),
  );

  return system_settings_form($form);
}
